package com.tsc.skuschenko.tm.configuration;

import com.tsc.skuschenko.tm.endpoint.ProjectEndpoint;
import com.tsc.skuschenko.tm.endpoint.TaskEndpoint;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;

@EnableWebMvc
@Configuration
@ComponentScan("com.tsc.skuschenko.tm")
public class WebApplicationConfiguration implements WebMvcConfigurer,
        WebApplicationInitializer {

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Override
    public void onStartup(@NotNull final ServletContext servletContext)
            throws ServletException {
        @NotNull final CXFServlet cxfServlet = new CXFServlet();
        @NotNull final ServletRegistration.Dynamic dynamic =
                servletContext.addServlet("cxfServlet", cxfServlet);
        dynamic.addMapping("/ws/*");
        dynamic.setLoadOnStartup(1);
    }

    @Bean
    @NotNull
    public Endpoint projectEndpointRegistry(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint =
                new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public ViewResolver resolver() {
        @NotNull final InternalResourceViewResolver resolver =
                new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    @NotNull
    public Endpoint taskEndpointRegistry(
            @NotNull final TaskEndpoint controller,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, controller);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

}