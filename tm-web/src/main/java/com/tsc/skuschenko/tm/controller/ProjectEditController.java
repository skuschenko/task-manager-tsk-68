package com.tsc.skuschenko.tm.controller;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.UUID;

@Controller
public class ProjectEditController {

    @Autowired
    @NotNull
    private IProjectService projectService;

    @GetMapping("/project/create")
    @NotNull
    public String create() {
        @NotNull final Project project =
                new Project("Project" + UUID.randomUUID().toString());
        projectService.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    @NotNull
    public String delete(@PathVariable("id") @NotNull final String id) {
        projectService.removeById(id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    @NotNull
    public String edit(
            @ModelAttribute("project") @NotNull final Project project,
            @NotNull final BindingResult result
    ) {
        projectService.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    @NotNull
    public ModelAndView edit(@PathVariable("id") @NotNull final String id) {
        @Nullable final Project project = projectService.findById(id);
        return new ModelAndView("project-edit", "project", project);
    }

    @ModelAttribute("statuses")
    @NotNull
    public Status[] getStatuses() {
        return Status.values();
    }

}
