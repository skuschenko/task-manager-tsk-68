package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends AbstractRepository<Task> {

    @Modifying
    @Query("DELETE FROM Task e")
    void clearAll();

    @Query("SELECT e FROM Task e")
    @Nullable List<Task> findAll();

    @Query("SELECT e FROM Task e WHERE e.id = :id")
    @Nullable Task findTaskById(@Param("id") @NotNull String id);

    @Modifying
    @Query("DELETE FROM Task e WHERE e.id = :taskId")
    void removeById(@Param("taskId") @NotNull String taskId);

}
