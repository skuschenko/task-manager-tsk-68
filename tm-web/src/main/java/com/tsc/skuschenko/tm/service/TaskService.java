package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.repository.TaskRepository;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
@NoArgsConstructor
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Transactional
    @Override
    public void clearAll() {
        taskRepository.clearAll();
    }

    @Override
    @NotNull
    @Transactional
    public Task create(@Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Task task =
                new Task(name);
        save(task);
        return task;
    }

    @Override
    @Nullable
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @Nullable
    public Task findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return taskRepository.findTaskById(id);
    }

    @Override
    @Transactional
    public void remove(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void save(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        taskRepository.save(task);
    }

}
