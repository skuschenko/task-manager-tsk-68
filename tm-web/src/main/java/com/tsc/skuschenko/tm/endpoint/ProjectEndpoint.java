package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.endpoint.IProjectRestEndpoint;
import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
@WebService(
        endpointInterface = "com.tsc.skuschenko.tm.api.endpoint." +
                "IProjectRestEndpoint"
)
public class ProjectEndpoint implements IProjectRestEndpoint {

    @NotNull
    static final String CREATE_ALL_METHOD = "/createAll";

    @NotNull
    static final String CREATE_METHOD = "/create";

    @NotNull
    static final String DELETE_ALL_METHOD = "/deleteAll";

    @NotNull
    static final String DELETE_BY_ID_METHOD = "/deleteById/{id}";

    @NotNull
    static final String FIND_ALL_METHOD = "/findAll";

    @NotNull
    static final String FIND_BY_ID_METHOD = "/findById/{id}";

    @NotNull
    static final String SAVE_ALL_METHOD = "/saveAll";

    @NotNull
    static final String SAVE_METHOD = "/save";

    @Autowired
    @NotNull
    private IProjectService projectService;

    @Override
    @WebMethod
    @PostMapping(CREATE_METHOD)
    public void create(
            @WebParam(name = "project")
            @RequestBody @NotNull final Project project
    ) {
        projectService.save(project);
    }

    @Override
    @WebMethod
    @PostMapping(CREATE_ALL_METHOD)
    public void createAll(
            @WebParam(name = "projects")
            @RequestBody @NotNull final Collection<Project> projects
    ) {
        projects.forEach(projectService::save);
    }

    @Override
    @WebMethod
    @DeleteMapping(DELETE_ALL_METHOD)
    public void deleteAll(
            @WebParam(name = "projects")
            @RequestBody @NotNull final Collection<Project> projects
    ) {
        projects.forEach(item -> projectService.removeById(item.getId()));
    }

    @Override
    @WebMethod
    @DeleteMapping(DELETE_BY_ID_METHOD)
    public void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        projectService.removeById(id);
    }

    @Override
    @WebMethod
    @GetMapping(FIND_BY_ID_METHOD)
    public Project find(
            @WebParam(name = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping(FIND_ALL_METHOD)
    public Collection<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @WebMethod
    @PutMapping(SAVE_METHOD)
    public void save(
            @WebParam(name = "project")
            @RequestBody @NotNull final Project project
    ) {
        projectService.save(project);
    }

    @Override
    @WebMethod
    @PutMapping(SAVE_ALL_METHOD)
    public void saveAll(
            @WebParam(name = "projects")
            @RequestBody @NotNull final Collection<Project> projects
    ) {
        projects.forEach(projectService::save);
    }

}
