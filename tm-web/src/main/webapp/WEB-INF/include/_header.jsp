<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
	<head>
		<title>Task manager</title>
	</head>
	<style>
		h1 {
		font-size: 16px;
		}

		.table {
		border: 1px solid;
        }
        .table__header_tr__td {
		border: 1px solid;
		width: 100%;
		text-align: center;
        }
        .table__header_tr {
		display: inline-flex;
		font-size: 25px;
		width: 100%;
		text-align: right;
        }
        .model__body_tr {
		display: flex;
        }
        .td_1 {
		width: 20%;
        }
        .td_2 {
		width: 20%;
		}
		.td_3 {
		width: 10%;
		}
		.td_4 {
		width: 20%;
		}
		.td_5 {
		width: 10%;
		}
		.td_6 {
		width: 10%;
		}
		.td_7 {
		width: 5%;
		}
		.td_8 {
		width: 5%;
		}
		.table__body_tr__td {
            border: 1px solid gray;
            text-align: center;
        }
	</style>
	<body>
		<div class = "table">
			<div class = "table__header">
				<div class = "table__header_tr">
					<div class = "table__header_tr__td">
						Task manager
					</div>
					<div class = "table__header_tr__td">
						<a class="td__a" href="/projects">Projects</a>
						<a class="td__a" href="/tasks">Tasks</a>
					</div>
				</div>
			</div>
		</div>