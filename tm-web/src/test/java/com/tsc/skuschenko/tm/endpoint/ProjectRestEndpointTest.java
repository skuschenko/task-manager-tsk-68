package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.client.ProjectRestClient;
import com.tsc.skuschenko.tm.configuration.DataBaseConfiguration;
import com.tsc.skuschenko.tm.marker.IntegrationCategory;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class ProjectRestEndpointTest {

    @Before
    public void before() {
      //  deleteAll();
     //   create();
    }

   /* @Test
    public void Test() {
        @NotNull
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        DataBaseConfiguration.class
                );
        @NotNull
        EntityManager entityManager = context.getBean(EntityManager.class);
        @NotNull final ProjectRepository projectRepository =
                context.getBean(ProjectRepository.class, entityManager);
        final List<Project> projects = projectRepository.findAll();

        entityManager.close();
    }*/

    @Test
    @Category(IntegrationCategory.class)
    public void create() {
        @NotNull final Project project = new Project("pro");
        ProjectRestClient.client().create(project);
        @NotNull final Project projectNew =
                ProjectRestClient.client().find(project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(project.getId(), projectNew.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void createAll() {
        @NotNull final List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            projects.add(new Project("pro" + i));
        }
        ProjectRestClient.client().createAll(projects);
        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteAll() {
        @NotNull final List<Project> projects = findAllProjects();
        ProjectRestClient.client().deleteAll(projects);
        Assert.assertEquals(0, findAllProjects().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteById() {
        @NotNull final Project project = findAllProjects().get(0);
        ProjectRestClient.client().deleteById(project.getId());
        Assert.assertEquals(0, findAllProjects().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        findAllProjects();
    }

    @NotNull
    private List<Project> findAllProjects() {
        @Nullable final List<Project> projects =
                new ArrayList<>(ProjectRestClient.client().findAll());
        Assert.assertNotNull(projects);
        return projects;
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        @NotNull final List<Project> projects = findAllProjects();
        Assert.assertNotNull(projects);
        @NotNull final Project projectNew =
                ProjectRestClient.client().find(projects.get(0).getId());
        Assert.assertEquals(projectNew.getId(), projects.get(0).getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        @NotNull final Project project = findAllProjects().get(0);
        project.setDescription(UUID.randomUUID().toString());
        ProjectRestClient.client().save(project);
        Assert.assertEquals(
                project.getDescription(),
                findAllProjects().get(0).getDescription()
        );
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveAll() {
        @NotNull final List<Project> projects = findAllProjects();
        projects.forEach(item ->
                item.setDescription(UUID.randomUUID().toString())
        );
        ProjectRestClient.client().saveAll(projects);
        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

}
