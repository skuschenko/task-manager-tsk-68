package com.tsc.skuschenko.tm.listener.task;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.Task;
import com.tsc.skuschenko.tm.endpoint.TaskEndpoint;
import com.tsc.skuschenko.tm.event.ConsoleEvent;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class TaskCreateListener extends AbstractTaskListener {

    private static final String DESCRIPTION = "create new task";

    private static final String NAME = "task-create";

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskCreateListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        showParameterInfo("name");
        @NotNull final String name = TerminalUtil.nextLine();
        showParameterInfo("description");
        @NotNull final String description = TerminalUtil.nextLine();
        @Nullable final Task task = taskEndpoint
                .addTask(session, name, description);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

    @Override

    public String name() {
        return NAME;
    }
}
