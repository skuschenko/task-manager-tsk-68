package com.tsc.skuschenko.tm.listener.user;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.UserEndpoint;
import com.tsc.skuschenko.tm.event.ConsoleEvent;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class UserUpdateProfileListener extends AbstractUserListener {

    private static final String DESCRIPTION = "update profile of current user";

    private static final String NAME = "update-user-profile";

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(
            condition = "@userUpdateProfileListener.name() == #event.name"
    )
    public void handler(@NotNull final ConsoleEvent event) {
        showOperationInfo(NAME);
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        @NotNull final String userId
                = session.getUserId();
        showParameterInfo("first name");
        @NotNull final String firstName = TerminalUtil.nextLine();
        showParameterInfo("last name");
        @NotNull final String lastName = TerminalUtil.nextLine();
        showParameterInfo("middle name");
        @NotNull final String middleName = TerminalUtil.nextLine();
        userEndpoint.updateUser(
                userId, firstName, lastName, middleName
        );
    }

    @Override
    public String name() {
        return NAME;
    }

}
