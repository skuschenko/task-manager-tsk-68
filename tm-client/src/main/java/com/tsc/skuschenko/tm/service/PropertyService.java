package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IPropertyService;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    @Value("${version}")
    private String applicationVersion;

}
