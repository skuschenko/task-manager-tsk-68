package com.tsc.skuschenko.tm.api;

import com.tsc.skuschenko.tm.dto.SessionDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionEndpoint {

    void closeSession(@NotNull SessionDTO session);

    @Nullable
    SessionDTO openSession(@NotNull String login, @NotNull String password);

}
